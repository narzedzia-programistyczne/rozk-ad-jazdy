package com.narzedziaprogramistyczne.rozkladjazdy;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import static org.junit.Assert.*;

public class AppControllerTest {

    @Test
    void ApiTest()
    {
        String licencja = "";
        String wlasciwaLicencja = "CC-BY-SA - Studio JZK sp. z o.o., 2018-2019";
        try
        {
            URL url = new URL("https://przyjazdy.pl/api/miasta?key=markui000");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            BufferedReader input = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = input.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            JSONObject object = new JSONObject(sb.toString());
            connection.disconnect();
            licencja =  object.getString("License");
            assertEquals(wlasciwaLicencja, licencja);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
    }
}
