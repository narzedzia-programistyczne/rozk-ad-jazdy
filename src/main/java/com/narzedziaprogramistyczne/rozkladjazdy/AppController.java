package com.narzedziaprogramistyczne.rozkladjazdy;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Controller
public class AppController
{
    @RequestMapping("/")
    public String Welcome(Model model)
    {
        try
        {
            URL url = new URL("https://przyjazdy.pl/api/miasta?key=markui000");
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            BufferedReader input = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();

            String line = null;
            while ((line = input.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            JSONObject object = new JSONObject(sb.toString());
            connection.disconnect();
            model.addAttribute("licencja", object.getString("License"));
            model.addAttribute("data", object.getString("Data"));
            model.addAttribute("ip", object.getString("IP"));
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch(JSONException e)
        {
            e.printStackTrace();
        }
        System.out.println("AppController -> Welcome()");
        return "index";
    }
}
