package com.narzedziaprogramistyczne.rozkladjazdy;

class Miasta {
    String nazwa;
    String woj;
    String nwoj;
    String dostawca;
    String url;
    String api;

    public Miasta(String nazwa, String woj, String nwoj, String dostawca, String url, String api) {
        this.nazwa = nazwa;
        this.woj = woj;
        this.nwoj = nwoj;
        this.dostawca = dostawca;
        this.url = url;
        this.api = api;
    }

    @Override
    public String toString() {
        return "Miasta{" +
                "nazwa='" + nazwa + '\'' +
                ", woj='" + woj + '\'' +
                ", nwoj='" + nwoj + '\'' +
                ", dostawca='" + dostawca + '\'' +
                ", url='" + url + '\'' +
                ", api='" + api + '\'' +
                '}';
    }


}
